<?xml version="1.0" encoding="UTF-8" standalone="yes"?>

<!--
    
    All modifications to this unit type must be added here!

    The version number of the unit type template (utt) has the format X.Y.n, for example 2.3.0, where:
    -   X stands for the “utt framework version” – this digit should not be updated!
    -   Y stands for the version of the current utt, from Qmatic – this digit should not be updated!
    -   n is the serial number that should be updated as soon as the utt has been adapted/customised to better suit your needs.

      
    Version history in descending order:
    ====================================
    Version    Date         Comments                                  			  Signature
    =========  ===========  ===================================================== =========
	5.4.0.4      2015-05-18   Live Data Integration                            			droux
    5.4.0.1      2015-05-18   First Version                             				boudui

    =======================================================================================  

-->

<unit name="branchStatusInfo"
      frameworkVersion="2"
      unitVersion="5.4.0.8"
      type="DISPLAY_POINT"
      defaultNoUnits="1"
      maxNoUnits="1"
      description="Will save the branch status to global variables">

    <helpText>
        This is a help text
    </helpText>

    <parameters>
    
      <parameter name="unitId"
        type="UnitId"
        defaultValue="branchStatusInfo"
        label="Unit id"
        description="Unique id of the unit"
        sortOrder="1"
        readLevel="branch"
        privilege="install:adminAdmin"
        size="16"/>
		
		<parameter name="queues" 
          type="Queue" 
          label="Queues"
          description="Queues for which the data should be saved."
          multiSelection="true"
          sortOrder="3" 
          privilege="install:adminAdmin"
          mandatory="false">
        </parameter>
		<parameter name="logData"
          sortOrder="990"
          label="Write data in QAgent log (debug)"
          description="Note that this may make the log file grow! Use with caution!"
          defaultValue="true"
          type="Boolean"
          writeLevel="profile"/>
		  
		  <parameter name="serverIP"
          sortOrder="991"
		  type="String"
          label="Ip Address of Central Server"
          description="Please note this has to be set in order for Stats to upload"
          defaultValue="localhost:8080"
		  readLevel="profile"
          writeLevel="profile"/>
		  
		  <parameter name="lowCountUser"
          sortOrder="992"
		  type="String"
          label="Email Address for LowCount"
          description="The User that will recieve the low ticket event for the branch"
		  readLevel="branch"
          writeLevel="branch"/>
		  
		  <parameter name="errorUser"
          sortOrder="993"
		  type="String"
          label="Email Address for Branch Admin"
          description="The User that will receive and TP errors for the branch"
		  readLevel="branch"
          writeLevel="branch"/>

        <parameter name="unitName" 
          type="UnitName" 
          defaultValue="${unit:name} ${unit:index}"
          label="Unit name">
        </parameter>
    </parameters>

    <devices>
        <device id="1" name="StatusCollector" type="SW_MEDIA_HD_DISPLAY_POINT">
            <parameters>
                <parameter name="refreshInterval" type="Integer" description="The amount of minutes between surface application update checks">30</parameter>
            </parameters>
          <deviceEventHandlers>
            <deviceEventHandler handlerType="GROOVY" name="REFRESH_STAT_INFO"><![CDATA[
				import com.qmatic.qp.api.device.DeviceEvent
				import com.qmatic.qp.api.device.DeviceCommand
        		DeviceCommand displayCommand = new DeviceCommand(device.getId(), "UPDATE_STAT_INFO")
				displayCommand.setParameter("originType", "REFRESH_STAT_INFO")
				displayCommand.setParameter("commandTime", event.getEventTime())
				displayCommand.setParameter("branchId",device.getBranchId())

		        result = false
        		try {
          			result = device.executeCommand(displayCommand)    
        		} catch(e) {
          			result = false
        		}
				
				]]></deviceEventHandler>
          </deviceEventHandlers>
          <deviceCommandHandlers>
        
            <deviceCommandHandler handlerType="GROOVY" name="UPDATE_STAT_INFO"><![CDATA[
				import com.qmatic.qp.api.device.DeviceEvent
				import com.qmatic.qp.api.device.DeviceCommand
				import com.qmatic.qp.jiql.core.util.CommonUtil
				import org.joda.time.format.DateTimeFormat 
				import com.qmatic.qp.api.util.TimeUtil
				import com.qmatic.qp.api.connectors.dto.GlobalVariable

				import java.util.List
				unit = device.getUnit()

				Boolean logData =unit.getConfiguration().get("logData")
				
				if(logData) {
					device.log("INFO","  Command: UPDATE_STAT_INFO" + " Send from: " + command.getParameter("originType"))
				}
				timeZone = unit.getBranch().getTimeZone().getID()
				eventTimeString = TimeUtil.getCurrentTimeInZoneAsString(timeZone)
				branchTimeOffset = eventTimeString.substring(eventTimeString.length()-5,eventTimeString.length())
				eventTime =  TimeUtil.parseStringToLocalDateTime(eventTimeString)
				currentDate = DateTimeFormat.forPattern("YYYYMMdd").print(eventTime)

				eventHour = (DateTimeFormat.forPattern("HH").print(eventTime)).toInteger()
				eventMin = (DateTimeFormat.forPattern("mm").print(eventTime)).toInteger()
				eventSec = (DateTimeFormat.forPattern("ss").print(eventTime)).toInteger()
				timeSec = (eventHour*3600) + (eventMin*60) + eventSec
				currentTime = eventHour*100+eventMin

				branchId =  unit.getBranchId()
				
				if(logData) {
					device.log("INFO","  BranchData: Got Branch Id")
 				}
				try
				{
    			 branchCurrent = "cUrl http://localhost:8080/NedbankWebServlets/UpdateBranchInfo?BranchId=$branchId".execute().text
				}
				catch(e)
				{
					if(logData) {
						device.log("ERROR"," Error in Rest Call : " + e)
 					}
				}

				globalVar = new GlobalVariable()
				globalVar.setName("livedata_" + branchId)
				globalVar.setValue(branchCurrent)
				device.getResource("SYSTEM").setGlobalVariable( globalVar)

				if(logData) {
					device.log("INFO","  BranchData: " + branchCurrent)
					device.log("INFO","==========================              ==========================")
				}
							
					]]></deviceCommandHandler>
          </deviceCommandHandlers>
        </device>
      </devices>
      <unitEvents>
        <unitEvent name="PUBLIC.VISIT_CALL">
          <unitEventHandler type="GROOVY"><![CDATA[
	 			import com.qmatic.qp.api.device.DeviceEvent
				import com.qmatic.qp.api.device.DeviceCommand

				Boolean logData =unit.getConfiguration().get("logData")
				if(logData) {
					unit.log("INFO","========================== statToOpspanelInfo data ==========================")
					unit.log("INFO","  Event: PUBLIC.VISIT_CALL")
				}

        		device = unit.getDevice("StatusCollector")
        		DeviceCommand displayCommand = new DeviceCommand(device.getId(), "UPDATE_STAT_INFO")
        		displayCommand.setParameters(event.getParameters())
				displayCommand.setParameter("originType", "CALL")
				displayCommand.setParameter("commandTime", event.getEventTime())
		        result = false
        		try {
          			result = device.executeCommand(displayCommand)    
        		} catch(e) {
          			result = false
        		}
			]]></unitEventHandler>
        </unitEvent>
		<unitEvent name="PUBLIC.VISIT_CREATE">
          <unitEventHandler type="GROOVY"><![CDATA[
	 			import com.qmatic.qp.api.device.DeviceEvent
				import com.qmatic.qp.api.device.DeviceCommand

				Boolean logData =unit.getConfiguration().get("logData")
				if(logData) {
					unit.log("INFO","========================== statToOpspanelInfo data ==========================")
					unit.log("INFO","  Event: PUBLIC.VISIT_CREATE")
				}

        		device = unit.getDevice("StatusCollector")
        		DeviceCommand displayCommand = new DeviceCommand(device.getId(), "UPDATE_STAT_INFO")
        		displayCommand.setParameters(event.getParameters())
				displayCommand.setParameter("originType", "CREATE")
				displayCommand.setParameter("commandTime", event.getEventTime())
		        result = false
        		try {
          			result = device.executeCommand(displayCommand)    
        		} catch(e) {
          			result = false
        		}
            ]]></unitEventHandler>
        </unitEvent>
         <unitEvent name="PUBLIC.SERVICE_POINT_CLOSE">
          <unitEventHandler type="GROOVY"><![CDATA[
	 			import com.qmatic.qp.api.device.DeviceEvent
				import com.qmatic.qp.api.device.DeviceCommand

				Boolean logData =unit.getConfiguration().get("logData")
				if(logData) {
					unit.log("INFO","========================== statToOpspanelInfo data ==========================")
					unit.log("INFO","  Event: PUBLIC.SERVICE_POINT_CLOSE")
					
				}
				device = unit.getDevice("StatusCollector")
        		DeviceCommand displayCommand = new DeviceCommand(device.getId(), "UPDATE_STAT_INFO")
        		displayCommand.setParameters(event.getParameters())
				displayCommand.setParameter("originType", "CLOSE")
				displayCommand.setParameter("commandTime", event.getEventTime())
		        result = false
        		try {
          			result = device.executeCommand(displayCommand)    
        		} catch(e) {
          			result = false
        		}
  				
			]]></unitEventHandler>
        </unitEvent>
        <unitEvent name="PUBLIC.SERVICE_POINT_OPEN">
          <unitEventHandler type="GROOVY"><![CDATA[
	 			import com.qmatic.qp.api.device.DeviceEvent
				import com.qmatic.qp.api.device.DeviceCommand

				Boolean logData =unit.getConfiguration().get("logData")
				if(logData) {
					unit.log("INFO","========================== statToOpspanelInfo data ==========================")
					unit.log("INFO","  Event: PUBLIC.SERVICE_POINT_OPEN")
				}
				device = unit.getDevice("StatusCollector")
        		DeviceCommand displayCommand = new DeviceCommand(device.getId(), "UPDATE_STAT_INFO")
        		displayCommand.setParameters(event.getParameters())
				displayCommand.setParameter("originType", "SERVICE_POINT_OPEN")
				displayCommand.setParameter("commandTime", event.getEventTime())
				result = false
        		try {
          			result = device.executeCommand(displayCommand)    
        		} catch(e) {
          			result = false
        		}
  				
			]]></unitEventHandler>
        </unitEvent>
        <!--unitEvent name="PUBLIC.VISIT_END">
          <unitEventHandler type="GROOVY"><![CDATA[
	 			import com.qmatic.qp.api.device.DeviceEvent
				import com.qmatic.qp.api.device.DeviceCommand

				Boolean logData =unit.getConfiguration().get("logData")
				if(logData) {
					unit.log("INFO","========================== statToOpspanelInfo data ==========================")
					unit.log("INFO","  Event: PUBLIC.VISIT_END")
				}
				device = unit.getDevice("StatusCollector")
        		DeviceCommand displayCommand = new DeviceCommand(device.getId(), "UPDATE_STAT_INFO")
        		displayCommand.setParameters(event.getParameters())
				displayCommand.setParameter("originType", "END")
				displayCommand.setParameter("commandTime", event.getEventTime())
		        result = false
        		try {
          			result = device.executeCommand(displayCommand)    
        		} catch(e) {
          			result = false
        		}
  				
			]]></unitEventHandler>
        </unitEvent-->
		<!--unitEvent name="PUBLIC.VISIT_NOSHOW">
          <unitEventHandler type="GROOVY"><![CDATA[
	 			import com.qmatic.qp.api.device.DeviceEvent
				import com.qmatic.qp.api.device.DeviceCommand

				Boolean logData =unit.getConfiguration().get("logData")
				if(logData) {
					unit.log("INFO","========================== statToOpspanelInfo data ==========================")
					unit.log("INFO","  Event: PUBLIC.VISIT_NOSHOW")
				}
				device = unit.getDevice("StatusCollector")
        		DeviceCommand displayCommand = new DeviceCommand(device.getId(), "UPDATE_STAT_INFO")
        		displayCommand.setParameters(event.getParameters())
				displayCommand.setParameter("originType", "NOSHOW")
				displayCommand.setParameter("commandTime", event.getEventTime())
		        result = false
        		try {
          			result = device.executeCommand(displayCommand)    
        		} catch(e) {
          			result = false
        		}
  				
			]]></unitEventHandler>
        </unitEvent-->
        <!--unitEvent name="PUBLIC.VISIT_RECYCLE">
          <unitEventHandler type="GROOVY"><![CDATA[
	 			import com.qmatic.qp.api.device.DeviceEvent
				import com.qmatic.qp.api.device.DeviceCommand

				Boolean logData =unit.getConfiguration().get("logData")
				if(logData) {
					unit.log("INFO","========================== statToOpspanelInfo data ==========================")
					unit.log("INFO","  Event: PUBLIC.VISIT_RECYCLE")
				}
				device = unit.getDevice("StatusCollector")
        		DeviceCommand displayCommand = new DeviceCommand(device.getId(), "UPDATE_STAT_INFO")
        		displayCommand.setParameters(event.getParameters())
				displayCommand.setParameter("originType", "RECYCLE")
				displayCommand.setParameter("commandTime", event.getEventTime())
		        result = false
        		try {
          			result = device.executeCommand(displayCommand)    
        		} catch(e) {
          			result = false
        		}
  				
			]]></unitEventHandler>
        </unitEvent-->
        <!--unitEvent name="PUBLIC.VISIT_REMOVE">
          <unitEventHandler type="GROOVY"><![CDATA[
	 			import com.qmatic.qp.api.device.DeviceEvent
				import com.qmatic.qp.api.device.DeviceCommand

				Boolean logData =unit.getConfiguration().get("logData")
				if(logData) {
					unit.log("INFO","========================== statToOpspanelInfo data ==========================")
					unit.log("INFO","  Event: PUBLIC.VISIT_REMOVE")
				}
				device = unit.getDevice("StatusCollector")
        		DeviceCommand displayCommand = new DeviceCommand(device.getId(), "UPDATE_STAT_INFO")
        		displayCommand.setParameters(event.getParameters())
				displayCommand.setParameter("originType", "REMOVE")
				displayCommand.setParameter("commandTime", event.getEventTime())
		        result = false
        		try {
          			result = device.executeCommand(displayCommand)    
        		} catch(e) {
          			result = false
        		}
  				
			]]></unitEventHandler>
        </unitEvent-->
        <!--unitEvent name="PUBLIC.VISIT_TRANSFER_TO_QUEUE">
          <unitEventHandler type="GROOVY"><![CDATA[
	 			import com.qmatic.qp.api.device.DeviceEvent
				import com.qmatic.qp.api.device.DeviceCommand

				Boolean logData =unit.getConfiguration().get("logData")
				if(logData) {
					unit.log("INFO","========================== statToOpspanelInfo data ==========================")
					unit.log("INFO","  Event: PUBLIC.VISIT_TRANSFER_TO_QUEUE")
				}

				device = unit.getDevice("StatusCollector")
        		DeviceCommand displayCommand = new DeviceCommand(device.getId(), "UPDATE_STAT_INFO")
        		displayCommand.setParameters(event.getParameters())
				displayCommand.setParameter("originType", "TRANSFER")
				displayCommand.setParameter("commandTime", event.getEventTime())
		        result = false
        		try {
          			result = device.executeCommand(displayCommand)    
        		} catch(e) {
          			result = false
        		}
  				
			]]></unitEventHandler>
        </unitEvent-->
        <!--unitEvent name="PUBLIC.VISIT_TRANSFER_TO_SERVICE_POINT_POOL">
          <unitEventHandler type="GROOVY"><![CDATA[
	 			import com.qmatic.qp.api.device.DeviceEvent
				import com.qmatic.qp.api.device.DeviceCommand

				Boolean logData =unit.getConfiguration().get("logData")
				if(logData) {
					unit.log("INFO","========================== statToOpspanelInfo data ==========================")
					unit.log("INFO","  Event: PUBLIC.VISIT_TRANSFER_TO_SERVICE_POINT_POOL")
				}
				device = unit.getDevice("StatusCollector")
        		DeviceCommand displayCommand = new DeviceCommand(device.getId(), "UPDATE_STAT_INFO")
        		displayCommand.setParameters(event.getParameters())
				displayCommand.setParameter("originType", "TRANSFER")
				displayCommand.setParameter("commandTime", event.getEventTime())
		        result = false
        		try {
          			result = device.executeCommand(displayCommand)    
        		} catch(e) {
          			result = false
        		}
  				
			]]></unitEventHandler>
        </unitEvent-->
        <!--unitEvent name="PUBLIC.VISIT_TRANSFER_TO_USER_POOL">
          <unitEventHandler type="GROOVY"><![CDATA[
	 			import com.qmatic.qp.api.device.DeviceEvent
				import com.qmatic.qp.api.device.DeviceCommand

				Boolean logData =unit.getConfiguration().get("logData")
				if(logData) {
					unit.log("INFO","========================== statToOpspanelInfo data ==========================")
					unit.log("INFO","  Event: PUBLIC.VISIT_TRANSFER_TO_USER_POOL")
				}
				device = unit.getDevice("StatusCollector")
        		DeviceCommand displayCommand = new DeviceCommand(device.getId(), "UPDATE_STAT_INFO")
        		displayCommand.setParameters(event.getParameters())
				displayCommand.setParameter("originType", "TRANSFER")
				displayCommand.setParameter("commandTime", event.getEventTime())
		        result = false
        		try {
          			result = device.executeCommand(displayCommand)    
        		} catch(e) {
          			result = false
        		}
  				
			]]></unitEventHandler>
        </unitEvent-->
        <unitEvent name="RESET">
          <unitEventHandler type="GROOVY"><![CDATA[
  	 			import com.qmatic.qp.api.device.DeviceEvent
				import com.qmatic.qp.api.device.DeviceCommand

				import com.qmatic.qp.jiql.core.util.CommonUtil
				import org.joda.time.format.DateTimeFormat 
				import com.qmatic.qp.api.util.TimeUtil
				import com.qmatic.qp.api.connectors.dto.BranchVariable
				import com.qmatic.qp.api.connectors.dto.GlobalVariable
				import java.util.List

				Boolean logData =unit.getConfiguration().get("logData")
				if(logData) {
					unit.log("INFO","========================== statToOpspanelInfo data ==========================")
					unit.log("INFO","  Event: RESET")
				}

			    device = unit.getDevice("StatusCollector")
    			timeZone = unit.getBranch().getTimeZone().getID()
				eventTimeString = TimeUtil.getCurrentTimeInZoneAsString(timeZone)
				branchTimeOffset = eventTimeString.substring(eventTimeString.length()-5,eventTimeString.length())
				eventTime =  TimeUtil.parseStringToLocalDateTime(eventTimeString)
				currentDate = DateTimeFormat.forPattern("YYYYMMdd").print(eventTime)

				eventHour = (DateTimeFormat.forPattern("HH").print(eventTime)).toInteger()
				eventMin = (DateTimeFormat.forPattern("mm").print(eventTime)).toInteger()
				currentTime = eventHour*100+eventMin
				branchId =  unit.getBranchId()
				
				// variable name: branchStatusInfo_ + branchId 
				// branchInfo:	timezone@reset@create@logon@call@updated		
				// Example:		+0200@20150506,1322@20150506,1322@20150506,1322@20150506,1322@20150506,1322

  				branchCurrent = branchTimeOffset + "@" + currentDate + ","+ currentTime + "@19700101,0000@19700101,0000@19700101,0000@19700101,0000"
				if(logData) {
					unit.log("INFO","  BranchData: " + branchCurrent)
					unit.log("INFO","==========================              ==========================")
				}

				globalVar = new GlobalVariable()
				globalVar.setName("branchStatusInfo_" + branchId)
				globalVar.setValue(branchCurrent)
				device.getResource("SYSTEM").setGlobalVariable( globalVar)
	
				branchVar = new BranchVariable()
				branchVar.setName("branchStatusInfo_" + branchId)
				branchVar.setValue(branchCurrent)
				device.getResource("BRANCH").setBranchVariable(branchId.toInteger(), branchVar)

			]]></unitEventHandler>
        </unitEvent>
	<unitEvent name="PUBLIC.USER_SERVICE_POINT_SESSION_END">
          <unitEventHandler type="GROOVY"><![CDATA[
	 			import com.qmatic.qp.api.device.DeviceEvent
				import com.qmatic.qp.api.device.DeviceCommand

				Boolean logData =unit.getConfiguration().get("logData")
				if(logData) {
					unit.log("INFO","========================== statToOpspanelInfo data ==========================")
					unit.log("INFO","  Event: PUBLIC.USER_SERVICE_POINT_SESSION_END")
				}
				device = unit.getDevice("StatusCollector")
        		DeviceCommand displayCommand = new DeviceCommand(device.getId(), "UPDATE_STAT_INFO")
        		displayCommand.setParameters(event.getParameters())
				displayCommand.setParameter("originType", "SESSION_END")
				displayCommand.setParameter("commandTime", event.getEventTime())
		        result = false
        		try {
          			result = device.executeCommand(displayCommand)    
        		} catch(e) {
          			result = false
        		}
  				
			]]></unitEventHandler>
        </unitEvent>
        <!--unitEvent name="PUBLIC.USER_SERVICE_POINT_SESSION_START">
          <unitEventHandler type="GROOVY"><![CDATA[
	 			import com.qmatic.qp.api.device.DeviceEvent
				import com.qmatic.qp.api.device.DeviceCommand

				Boolean logData =unit.getConfiguration().get("logData")
				if(logData) {
					unit.log("INFO","========================== statToOpspanelInfo data ==========================")
					unit.log("INFO","  Event: PUBLIC.USER_SERVICE_POINT_SESSION_START")
				}
				device = unit.getDevice("StatusCollector")
        		DeviceCommand displayCommand = new DeviceCommand(device.getId(), "UPDATE_STAT_INFO")
        		displayCommand.setParameters(event.getParameters())
				displayCommand.setParameter("originType", "SESSION_START")
				displayCommand.setParameter("commandTime", event.getEventTime())
		        result = false
        		try {
          			result = device.executeCommand(displayCommand)    
        		} catch(e) {
          			result = false
        		}
  				
			]]></unitEventHandler>
        </unitEvent-->
        <!--unitEvent name="PUBLIC.USER_SERVICE_POINT_WORK_PROFILE_SET">
          <unitEventHandler type="GROOVY"><![CDATA[
	 			import com.qmatic.qp.api.device.DeviceEvent
				import com.qmatic.qp.api.device.DeviceCommand

				Boolean logData =unit.getConfiguration().get("logData")
				if(logData) {
					unit.log("INFO","========================== statToOpspanelInfo data ==========================")
					unit.log("INFO","  Event: PUBLIC.USER_SERVICE_POINT_WORK_PROFILE_SET")
				}
				device = unit.getDevice("StatusCollector")
        		DeviceCommand displayCommand = new DeviceCommand(device.getId(), "UPDATE_STAT_INFO")
        		displayCommand.setParameters(event.getParameters())
				displayCommand.setParameter("originType", "PROFILE_SET")
				displayCommand.setParameter("commandTime", event.getEventTime())
		        result = false
        		try {
          			result = device.executeCommand(displayCommand)    
        		} catch(e) {
          			result = false
        		}
  				
			]]></unitEventHandler>
        </unitEvent-->
        <unitEvent name="PUBLIC.USER_SESSION_END">
          <unitEventHandler type="GROOVY"><![CDATA[
	 			import com.qmatic.qp.api.device.DeviceEvent
				import com.qmatic.qp.api.device.DeviceCommand

				Boolean logData =unit.getConfiguration().get("logData")
				if(logData) {
					unit.log("INFO","========================== statToOpspanelInfo data ==========================")
					unit.log("INFO","  Event: PUBLIC.USER_SESSION_END")
				}
				device = unit.getDevice("StatusCollector")
        		DeviceCommand displayCommand = new DeviceCommand(device.getId(), "UPDATE_STAT_INFO")
        		displayCommand.setParameters(event.getParameters())
				displayCommand.setParameter("originType", "USER_SESSION_END")
				displayCommand.setParameter("commandTime", event.getEventTime())
		        result = false
        		try {
          			result = device.executeCommand(displayCommand)    
        		} catch(e) {
          			result = false
        		}
  				
			]]></unitEventHandler>
        </unitEvent>
        <unitEvent name="PUBLIC.USER_SESSION_START">
          <unitEventHandler type="GROOVY"><![CDATA[
	 			import com.qmatic.qp.api.device.DeviceEvent
				import com.qmatic.qp.api.device.DeviceCommand

				Boolean logData =unit.getConfiguration().get("logData")
				if(logData) {
					unit.log("INFO","========================== statToOpspanelInfo data ==========================")
					unit.log("INFO","  Event: PUBLIC.VISIT_SESSION_START")
				}

        		device = unit.getDevice("StatusCollector")
        		DeviceCommand displayCommand = new DeviceCommand(device.getId(), "UPDATE_STAT_INFO")
        		displayCommand.setParameters(event.getParameters())
				displayCommand.setParameter("originType", "LOGON")
				displayCommand.setParameter("commandTime", event.getEventTime())
		        result = false
        		try {
          			result = device.executeCommand(displayCommand)    
        		} catch(e) {
          			result = false
        		}
            ]]></unitEventHandler>
        </unitEvent>
        <!--unitEvent name="PUBLIC.VISIT_CONFIRM">
          <unitEventHandler type="GROOVY"><![CDATA[
	 			import com.qmatic.qp.api.device.DeviceEvent
				import com.qmatic.qp.api.device.DeviceCommand

				Boolean logData =unit.getConfiguration().get("logData")
				if(logData) {
					unit.log("INFO","========================== statToOpspanelInfo data ==========================")
					unit.log("INFO","  Event: PUBLIC.VISIT_CONFIRM")
				}
				device = unit.getDevice("StatusCollector")
        		DeviceCommand displayCommand = new DeviceCommand(device.getId(), "UPDATE_STAT_INFO")
        		displayCommand.setParameters(event.getParameters())
				displayCommand.setParameter("originType", "VISIT_CONFIRM")
				displayCommand.setParameter("commandTime", event.getEventTime())
		        result = false
        		try {
          			result = device.executeCommand(displayCommand)    
        		} catch(e) {
          			result = false
        		}
  				
			]]></unitEventHandler>
        </unitEvent-->
        <!--unitEvent name="PUBLIC.VISIT_NEXT">
          <unitEventHandler type="GROOVY"><![CDATA[
	 			import com.qmatic.qp.api.device.DeviceEvent
				import com.qmatic.qp.api.device.DeviceCommand

				Boolean logData =unit.getConfiguration().get("logData")
				if(logData) {
					unit.log("INFO","========================== statToOpspanelInfo data ==========================")
					unit.log("INFO","  Event: PUBLIC.VISIT_NEXT")
				}
				device = unit.getDevice("StatusCollector")
        		DeviceCommand displayCommand = new DeviceCommand(device.getId(), "UPDATE_STAT_INFO")
        		displayCommand.setParameters(event.getParameters())
				displayCommand.setParameter("originType", "NEXT")
				displayCommand.setParameter("commandTime", event.getEventTime())
		        result = false
        		try {
          			result = device.executeCommand(displayCommand)    
        		} catch(e) {
          			result = false
        		}
 			]]></unitEventHandler>
        </unitEvent-->
      </unitEvents>

</unit>
